# SEQUSS
The project contains the implementation of the SEQUESS quality assessment metric.
If you use this software for your research, please cite the following paper:

- Saeed Mahmoudpour and Peter Schelkens, "Unifying Structural and Semantic Similarities for Quality Assessment of DIBR-synthesized Views", IEEE Access, 2022.
