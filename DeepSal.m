function [scm, scs] = DeepSal(Ref,Dist,net2)

relu_resnet = [5 17 79 141 173 175]; %Resnet %175 is the FC layer
c2 = 1e-6;
lay2 = net2.Layers;

ind = relu_resnet(1,3);
laynom = lay2(ind,1).Name;
act_Ref = activations(net2,Ref,laynom);
act_Dis = activations(net2,Dist,laynom);

s1 = std(act_Ref(:));
s2 = std(act_Dis(:));
s12 = cov(act_Ref(:) , act_Dis(:));
s12=s12(1,2);

scm = (2.*s12 + c2)./ (s1.^2 + s2.^2 + c2);



%%%%%%%%%%%%%%%The semantic fromFC layer
ind = relu_resnet(1,6);
laynom = lay2(ind,1).Name;
act_Ref = activations(net2,Ref,laynom);
act_Dis = activations(net2,Dist,laynom);
act_Ref =  double(act_Ref(:));
act_Dis=  double(act_Dis(:));
act_Ref=act_Ref';
act_Dis=act_Dis';


scs = pdist2(act_Ref,act_Dis,'spearman');

end