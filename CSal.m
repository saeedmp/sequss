function  [Ref1, Dist1,map1] =  CSal(Ref,Dist,net2)
relu_resnet = [5 17 79 141 173 175]; %Resnet %175 is the FC layer

lay2 = net2.Layers;

Ref1= Ref;
Dist1 = Dist;

ind = relu_resnet(1,5);
laynom = lay2(ind,1).Name;
sal1=0;

act_Ref = activations(net2,Ref,laynom);

for iz = 1:2048
    xx = act_Ref(:,:,iz);
    
%      figure()
%      imagesc(xx)
%  colormap(gray)
%  set(gcf, 'Position',  [100, 100, 500, 400])
    J = imresize(xx,32);

    sal1 = J+sal1;
    
    
end
mmm1 = mean(sal1(:)) ;

map1 = sal1;

sal1(sal1<mmm1)=0;

sal1=cat(3,sal1,sal1,sal1);


Ref1(sal1==0)=0;
Dist1(sal1==0)=0;
%%%%%%%%%%%%%%%%%%%%%End Saliency

end