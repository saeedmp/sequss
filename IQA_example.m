%Install resent50 before running the code
%Qf: Final quality score
%scor1: quality score
%scor2: semantic score
%Sal_scor1: quality score of the salient region
%Sal_scor2: semantic score of the salient region

net = resnet50()

relu_resnet = [5 17 79 141 173 175]; %Resnet ... 175 is index of the FC layer


databasepath_Ref = './kendo_Cam03_10.bmp';
databasepath_Dis = './kendo_05_to_03_10_ahn.png';

DistF = imread(databasepath_Dis);
RefF = imread(databasepath_Ref);
[rows, columns, numberOfColorChannels] = size(RefF);

if (rows>columns)

    Ref1 = imresize(RefF,[NaN 224]);
    Dist1 = imresize(DistF,[NaN 224]);

else
    Ref1 = imresize(RefF,[224 NaN]);
    Dist1 = imresize(DistF,[224 NaN]);
end


%%%%%%%%Compute Quaity/Semantic features
Ref = Ref1(1:224,1:224,1:3);
Dist = Dist1(1:224,1:224,1:3);

[scm1, scs1] = DeepF(Ref,Dist,net);

Ref = Ref1(end-223:end,end-223:end,1:3);
Dist = Dist1(end-223:end,end-223:end,1:3);

[scm2, scs2] = DeepF(Ref,Dist,net);

scor1 = (scm1+scm2)./2;
scor2 = (scs1+scs2)./2;

%%%%%%%%%%%%%%%%%%%%%%%%Saliency calculation

if (rows>columns)

    Ref1 = imresize(RefF,[NaN 224]);
    Dist1 = imresize(DistF,[NaN 224]);
    le = length(Ref1);
    Ref = Ref1(le/2-112:le/2+111,1:224,1:3);
    Dist = Dist1(le/2-112:le/2+111,1:224,1:3);
else
    Ref1 = imresize(RefF,[224 NaN]);
    Dist1 = imresize(DistF,[224 NaN]);
    le = length(Ref1);
    Ref = Ref1(1:224,le/2-111:le/2+112,1:3);
    Dist = Dist1(1:224,le/2-111:le/2+112,1:3);
end


%%Compute Saliency
[Ref1, Dist1,map1] =  CSal(Ref,Dist,net);

% compute quality and semantic scores for the salient region
[Sal_scm, Sal_scs] = DeepSal(Ref1,Dist1,net);

Sal_scor1 = Sal_scm;
Sal_scor2 = Sal_scs;


%%combine quality-semantic scores
Qm = 0.5.*scor1+ 0.5.*(1-scor2);
Qm_sal = 0.6.*Sal_scor1+ 0.4.*(1-Sal_scor2);


Qf = 0.6.*Qm + 0.4.*Qm_sal
